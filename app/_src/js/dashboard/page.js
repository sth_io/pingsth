var app = angular.module('noteSth');
app.controller('page', ['$scope', 'dataS', '$cookieStore', '$location',

function($scope, dataS, $cookieStore, $location) {
    $today = new Date();
    $yesterday = new Date($today);
    $yesterday.setDate($today.getDate() - 1);
    var id = '';
    if ($location.$$path.indexOf('/page') > -1) {
        id = $location.$$path.substr(6, $location.$$path.length);
    }

    if (id.length > 0) {
        dataS.getData('/websites/' + id, true)
            .success(function(data) {
                $scope.website = data;
                acessibility(data);
                $scope.inter = data.timeout;
                getXRecords(data, $yesterday);
            });
    }
    $scope.acessibility;
    acessibility = function(website) {
        var val = website.up / (website.up + website.down) * 100;
        if (val > 90) {
            $scope.acessibility = 'ok';
        }
        if (val >= 60 && val <= 90) {
            $scope.acessibility = 'mid';
        }
        if (val < 60) {
            $scope.acessibility = 'bad';
        }
    }

    $scope.chartconf = {
        legend: {
            display: false,
            position: 'left',
            // you can have html in series name
            htmlEnabled: false,
            waitForHeightAndWidth: false
        }
    }


    $scope.response = {
        load: function() {
            $scope.responses = {
                data: [],
                series: [""]
            };
            getXRecords($scope.website, $scope.response.from, $scope.response.to);
        }
    }


    $scope.responses = {
        data: [],
        series: [""]
    };
    var indexes = 0;

    function getXRecords(website, fr, to) {
        var dateObj = new Date(fr);
        var month = dateObj.getUTCMonth() + 1; //months from 1-12
        var day = dateObj.getUTCDate();
        var year = dateObj.getUTCFullYear();
        var x = year + ',' + (month-1) + ',' + day + ',0,0,0';
        var y = year + ',' + (month-1) + ',' + day + ',23,59,59';

    dataS.getData('/status/' + website._id + '/' + x + '/' + y, true)
        .success(function(data) {
            if (data.length > 1) {
                for (var i = 0, len = data.length; i < len; i++) {
                    if (data[i] && data[i].hasOwnProperty('website') && data[i].response != null && i % (10/$scope.inter) == 0) {
                        $scope.responses.data.push({
                            x: [data[i].timestamp],
                            y: [data[i].response]
                        });
                        indexes++;
                    }
                }
            }

        })
}

}]);
